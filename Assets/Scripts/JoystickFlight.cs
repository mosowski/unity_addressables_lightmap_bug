﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TouchJoysticks
{
    public class JoystickFlight : MonoBehaviour
    {
        public Joystick lookJoystick;
        public Joystick moveJoystick;
        public float rotationSpeed = 1;
        public float moveSpeed = 1;

        void Start () {
            
        }
        
        // Update is called once per frame
        void Update () {
            if (lookJoystick != null && lookJoystick.IsPressed)
            {
                transform.Rotate(Vector3.up, lookJoystick.Direction.x, Space.World);
                transform.Rotate(Vector3.left, lookJoystick.Direction.y, Space.Self);
            }
            if (moveJoystick != null && moveJoystick.IsPressed)
            {
                transform.Translate(moveJoystick.Direction.x, 0, moveJoystick.Direction.y, Space.Self);
            }
        }
    }

}
