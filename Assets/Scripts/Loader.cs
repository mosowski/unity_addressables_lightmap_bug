﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
	public string levelAddress;

	public void OnLoadClicked()
	{
		Addressables.LoadScene(levelAddress, LoadSceneMode.Additive).Completed += operation => transform.parent.gameObject.SetActive(false);
	}
}
