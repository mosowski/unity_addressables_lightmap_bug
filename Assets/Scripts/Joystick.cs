﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TouchJoysticks
{
    public class Joystick : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {

        public GameObject handle;
        public GameObject bg;
        
        private float radius;
        private Vector3 initialLocation;
        private Vector2 direction;
        private bool isPressed;

        public Vector2 Direction => direction;
        public Vector2 NormalizedDirection => direction.normalized;
        public bool IsPressed => isPressed;
        private RectTransform rectTransform;
        
        void Start ()
        {
            initialLocation = transform.position;
            rectTransform = GetComponent<RectTransform>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            isPressed = true;
        }

        public void OnDrag(PointerEventData eventData)
        {
            radius = rectTransform.rect.width * 0.5f * rectTransform.lossyScale.x;
            bg.transform.position = eventData.pressPosition;
            
            var delta = eventData.position - eventData.pressPosition;
            delta = delta.normalized * Mathf.Min(delta.magnitude, radius);
            direction = delta / radius;
            handle.transform.position = eventData.pressPosition + delta;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            bg.transform.position = initialLocation;
            handle.transform.position = initialLocation;
            isPressed = false;
        }
    }

}
